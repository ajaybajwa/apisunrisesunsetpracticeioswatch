//
//  InterfaceController.swift
//  APIDemo WatchKit Extension
//
//  Created by Parrot on 2019-03-03.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import Alamofire
import SwiftyJSON

@available(watchOSApplicationExtension 6.0, *)
class InterfaceController: WKInterfaceController {

    
    @IBOutlet var imgLoading: WKInterfaceImage!
    
    @IBOutlet var imgSunsetloading: WKInterfaceImage!
    @IBOutlet var imgSunriseLoading: WKInterfaceImage!
    @IBOutlet var txtCity: WKInterfaceTextField!
    @IBOutlet var lblSunrise: WKInterfaceLabel!
    @IBOutlet var lblSunset: WKInterfaceLabel!
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        
        // 1. start the animation
            self.imgSunriseLoading.setImageNamed("Progress")
            self.imgSunriseLoading.startAnimatingWithImages(in: NSMakeRange(0, 10), duration: 2, repeatCount: 0)
            
            self.imgSunsetloading.setImageNamed("Progress")
            self.imgSunsetloading.startAnimatingWithImages(in: NSMakeRange(0, 10), duration: 2, repeatCount: 0)
        
        let sharedPreferences = UserDefaults.standard
        var city = sharedPreferences.string(forKey: "city")

        if (city == nil) {
            // by default, the strating city is Vancouver
            city = "Vancouver"
            print("No city was set, setting default city to Vancouver")
        }
        else {
            print("I found a city: \(city)")
        }
        
        // update the label to show the current city
        self.txtCity.setText(city)
        
        //lat long
        var lat = sharedPreferences.string(forKey: "latitude")
               var lng = sharedPreferences.string(forKey: "longitude")
               
               if (lat == nil || lng == nil) {
                   // if lat/lng is missing then use default coordinates
                   // vancouver
                   lat = "45.7411"
                   lng = "-71.8523"
               }
        print(lat)
        print(lng)

        
        self.imgLoading.setImageNamed("Progress")
        self.imgLoading.startAnimatingWithImages(in: NSMakeRange(0, 10), duration: 2, repeatCount: 5)
        
        let URL = "https://api.sunrise-sunset.org/json?lat=\(lat!)&lng=\(lng!)&date=today"

        Alamofire.request(URL).responseJSON{
            response in
            guard let apiData = response.result.value else{
                print("Error getting response from url")
                return
            }
            print(apiData)
            
            let jsonResponse = JSON(apiData)
            let sunriseTime = jsonResponse["results"]["sunrise"].string
            let sunsetTime = jsonResponse["results"]["sunset"].string
            print("sunrise : \(sunriseTime)")
            print("sunset : \(sunsetTime)")
            
                       
             // stop the animation
            self.imgLoading.stopAnimating()
            // and then hide it
            self.imgLoading.setImageNamed(nil)
            
            // and then hide the animation
                       self.imgSunriseLoading.setImageNamed(nil)
                       self.imgSunsetloading.setImageNamed(nil)
            
            self.lblSunrise.setText(sunriseTime)
            self.lblSunset.setText(sunsetTime)
        }
        
        // TODO: Put your API call here
    }
   
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    @IBAction func btnGetData() {
        
    }
    
    @IBAction func btnChangeCity() {
        // 1. When person clicks on button, show them the input UI
                let suggestedResponses = ["Toronto", "Montreal","New York City","Los Angeles"]
                presentTextInputController(withSuggestions: suggestedResponses, allowedInputMode: .plain) {
        
                    (results) in
        
                    if (results != nil && results!.count > 0) {
                        // 2. write your code to process the person's response
                        let userResponse = results?.first as? String
                        self.txtCity.setText(userResponse)
                    }
                }

    }
}

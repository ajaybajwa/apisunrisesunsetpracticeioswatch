//
//  ViewController.swift
//  APIDemo
//
//  Created by Parrot on 2019-03-03.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController {

    @IBOutlet weak var lblSunset: UILabel!
    @IBOutlet weak var lblSunrise: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let URL = "https://api.sunrise-sunset.org/json?lat=49.2827&lng=-123.1207&date=today"
        Alamofire.request(URL).responseJSON {
            response in
            
            // TODO: Put your code in here
            // ------------------------------------------
            // 1. Convert the API response to a JSON object
            // 2. Parse out the data you need (sunrise / sunset time)
            // 3. Show the data in the user interface
            // 2. get the data out of the variable
             guard let apiData = response.result.value else {
                 print("Error getting data from the URL")
                 return
             }
             
            print(apiData)
            
            
            let jsonResponse = JSON(apiData)
                      let sunriseTime = jsonResponse["results"]["sunrise"].string
                      let sunsetTime = jsonResponse["results"]["sunset"].string
                      
                      print("Sunrise: \(sunriseTime)")
                      print("Sunset: \(sunsetTime)")
            
            self.lblSunset.text = sunsetTime!
            self.lblSunrise.text = sunriseTime!
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

